<?php

use \Symfony\Component\HttpFoundation\Request;

$signature = $app['controllers_factory'];

$signature->post('/signature', function (Request $request) use ($app) {

    $data = json_decode($request->getContent(), true);

    $returnValid = $app['signatureService']->valid($data, $app['validator']);

    if ($returnValid['status'] === 'true') {
        $return = $app['signatureService']->signature($data, $app['dbs']);

        if ($return['status'] == 'true') {
            $payload = ['status' => true, 'data' => $return['data'], 'message' => $return['message']];
            $code = 200;

            return $app->json([$payload], $code);
        } else {
            $payload = ['status' => false, 'data' => $return['data'], 'message' => $return['message']];
            $code = 404;

            return $app->json([$payload], $code);
        }
    } else {
        $payload = ['errors' => 'Request fields cannot be empty or do not exist!'];
        $code = 400;

        return $app->json([$payload], $code);
    }
});

return $signature;