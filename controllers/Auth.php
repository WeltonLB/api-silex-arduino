<?php

use \Symfony\Component\HttpFoundation\Request;

$auth = $app['controllers_factory'];

// Autenticacao
$auth->post('/login', function (Request $request) use ($app) {

     $data = json_decode($request->getContent(), true);
// verifica usuário no banco de dados

    $returnValid = $app['authService']->valid($data, $app['validator']);

    if($returnValid['status'] === 'true'){
        $user = $app['authService']->isValidUser($data, $app['dbs']);

        if ($user['status']) {
            $jwt = JWTWrapper::encode([
                'expiration_sec' => 3600,
                'iss' => 'crediok.com.br',
                'userdata' => ''
            ]);
            $payload = ['login' => true, 'access_token' => $jwt, 'data' => $user['data'], 'message' => 'User successfully authenticated!'];
            $code = 200;

            return $app->json([$payload],$code);

        } else {
            $payload = ['errors' => 'Invalid user or password!'];
            $code = 401;

            return $app->json([$payload],$code);
        }
    }else {

        $payload = ['errors' => 'Request fields cannot be empty or do not exist!'];
        $code = 400;

        return $app->json([$payload], $code);
    }

});

$auth->post('/token', function (Request $request) use ($app) {
    return $app->json([
        'status' => true,
        'message' => 'Token válido',
    ]);
});
return $auth;