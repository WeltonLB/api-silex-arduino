<?php


namespace Code\Sistema\Service;

use Code\Sistema\Entity\Auth;
use Code\Sistema\Mapper\AuthMapper;
use Symfony\Component\Validator\Constraints as Assert;

class AuthService
{
    private $auth;
    private $authMapper;

    public function __construct(Auth $auth, AuthMapper $authMapper)
    {
        $this->auth = $auth;
        $this->authMapper = $authMapper;
    }

    public function isValidUser(array $data, $db = null)
    {
        $auth = $this->auth;
        $auth->setUsuario($data['user']);
        $auth->setSenha($data['password']);

        $mapper = $this->authMapper;

        $result = $mapper->isValidUser($auth, $db);

        if (isset($result['count'])) {
            if ($result['count'] > 1) {
                return ['status' => true, 'data' => $result['data']];
            } else {
                return ['status' => false];
            }
        } else {
            return ['status' => false];
        }
    }

    public function valid(array $data, $valid = null){
        $outputErrors = '';

        $constraint = new Assert\Collection(array
        (
            'user' => new Assert\Required(
                array(new Assert\NotBlank(), new Assert\NotNull())),
            'password' => new Assert\Required(
                array(new Assert\NotBlank(), new Assert\NotNull())),
        ));

        $errors = $valid->validate($data, $constraint);

        if (count($errors) > 0) {

            foreach ($errors as $error) {
                $outputErrors .= $error->getPropertyPath() . ' -> ' . $error->getMessage() . ";";
            }

            return ['status' => 'false', 'errors' => explode(";", rtrim($outputErrors, ';'))];

        } else {
            return ['status' => 'true'];
        }
    }
}