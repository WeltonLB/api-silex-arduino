<?php
/**
 * Created by PhpStorm.
 * User: welton.bonfim
 * Date: 09/10/2017
 * Time: 10:12
 */

namespace Code\Sistema\Service;


use Symfony\Component\Validator\Constraints as Assert;


class SignatureService
{

    public function valid(array $data, $valid = null)
    {
        $outputErrors = '';

        $constraint = new Assert\Collection(array
        (
            'nuCPFCNPJ' => new Assert\Required(
                array(new Assert\NotBlank(), new Assert\NotNull())),
            'endEletronicoSacadorAvalista' => new Assert\Required(
                array(new Assert\NotNull())),
         ));

        $errors = $valid->validate($data, $constraint);

        if (count($errors) > 0) {

            foreach ($errors as $error) {
                $outputErrors .= $error->getPropertyPath() . ' -> ' . $error->getMessage() . ";";
            }
            return ['status' => 'false', 'errors' => explode(";", rtrim($outputErrors, ';'))];

        } else {
            return ['status' => 'true'];
        }
    }
}