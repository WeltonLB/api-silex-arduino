<?php
/**
 * Created by PhpStorm.
 * User: wladson.lima
 * Date: 24/03/2017
 * Time: 11:59
 */

namespace Code\Sistema\Entity;


class Auth
{
    private $usuario;
    private $senha;

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

}