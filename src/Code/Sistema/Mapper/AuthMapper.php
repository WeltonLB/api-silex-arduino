<?php

namespace Code\Sistema\Mapper;

use Code\Sistema\Entity\Auth;

class AuthMapper
{
    public function isValidUser(Auth $auth, $db = null)
    {
        $db['db']->beginTransaction();

        try {
            $sql = " SELECT id_user AS ID, user_api AS 'USER' FROM api_users 
                     WHERE user_api = ? 
                     AND password_api = ? ";
            $stmt = $db['db']->prepare($sql);
            $stmt->bindValue(1, $auth->getUsuario());
            $stmt->bindValue(2, $auth->getSenha());

            $stmt->execute();
            $retorno = $stmt->fetch();
            $db['db']->commit();

            return ['status' => true, 'data' => $retorno, 'count' => count($retorno)];

        } catch (\Exception $e) {
            $db['db']->rollBack();
            throw $e;
            return ['status' => false, 'error' => $e];
        }
    }
}