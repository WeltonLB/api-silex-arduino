<?php

require_once __DIR__ . '/../bootstrap.php';
require_once __DIR__ . '/../src/JWTWrapper.php';

use \Symfony\Component\HttpFoundation\Request;

/*
 * Auth
 */

$authController = include __DIR__ . '/../controllers/Auth.php';
$app->mount('/auth', $authController);

$authController = include __DIR__ . '/../controllers/Auth.php';
$app->mount('/valid', $authController);

/*
 * Contract
 */

$signatureController = include __DIR__ . '/../controllers/Signature.php';
$app->mount('/register', $signatureController);


$app->before(function (Request $request, $app) {

    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
    $route = $request->get('_route');

    if ($route != 'POST_auth_login') {
        $authorization = $request->headers->get("Authorization");
        list($jwt) = sscanf($authorization, 'Bearer %s');
        if ($jwt) {
            try {
                $app['jwt'] = JWTWrapper::decode($jwt);
            } catch (Exception $ex) {

                if($ex->getMessage() === "Expired token"){
                    $payload = ['errors' => 'Expired token!'];
                    $code = 401;

                    return $app->json([$payload],$code);
                } else {
                    $payload = ['errors' => 'Invalid token!'];
                    $code = 401;

                    return $app->json([$payload],$code);
                }
            }
        } else {
            $payload = ['error' => 'Access token not informed!'];
            $code = 401;

            return $app->json([$payload],$code);
        }
    }
});

$app->run();