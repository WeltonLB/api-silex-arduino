<?php

require_once 'vendor/autoload.php';

$app = new \Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
            'db' => array(
            'driver' => 'pdo_sqlsrv',
            'host' => 'srvdcsql.database.windows.net',
            'dbname' => 'api-arduino',
            'user' => 'administrador',
            'password' => 'weltonLima12',
            'charset' => 'UTF-8',
        )
    ),

));

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

	
$app->register(new Silex\Provider\ValidatorServiceProvider());

use Code\Sistema\Service\AuthService;
use Code\Sistema\Entity\Auth;
use Code\Sistema\Mapper\AuthMapper;

$app['authService'] = function () {

    $authEntity = new  Auth();
    $authMapper = new AuthMapper();
    $authService = new AuthService($authEntity, $authMapper);
    return $authService;
};

use Code\Sistema\Service\SignatureService;
use Code\Sistema\Entity\Signature;


$app['signatureService'] = function () {


    $signatureEntity = new  Signature();
    $signatureService = new SignatureService($signatureEntity);
    return $signatureService;
};

$app['swiftmailer.options'] = array(
    'host' => '',
    'port' => '',
    'username' => '',
    'password' => '',
    'encryption' => null,
    'auth_mode' => null
);